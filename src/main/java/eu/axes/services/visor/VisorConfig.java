package eu.axes.services.visor;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * This class is a simple holder for the configuration parameter of each collection to be used in Visor indexing
 *
 * @author ymombrun
 * @date 2014-02-24
 */
public class VisorConfig {


	private Map<String, String> env;


	private File datasetsFolder;


	private String commandLine;


	private File workingDirectory;


	private int exitValue;


	private long timeout;


	private boolean removeKeyframePrefix;


	private final Log log = LogFactory.getLog(this.getClass());


	/**
	 * @return <code>true</code> if every needed values are provided and valid
	 */
	public boolean isValid() {
		if (this.commandLine == null) {
			this.log.error("Required parameter commandLine is missing for VisorConfig.");
			return false;
		}
		return this.isFolderValid(this.datasetsFolder, "datasetsFolder") && this.isFolderValid(this.workingDirectory, "workingDirectory");
	}


	/**
	 * @param folder
	 *            The path to check that it is an existing directory
	 * @param folderName
	 *            The name of that directory used for logging purposes.
	 * @return <code>true</code> If and only if that folder already exists as a directory.
	 */
	private boolean isFolderValid(final File folder, final String folderName) {
		if (folder == null) {
			this.log.error("Visor " + folderName + " parameter is missing but compulsory.");
			return false;
		}
		if (!folder.isAbsolute()) {
			this.log.warn("Visor " + folderName + folder.getAbsolutePath() + ") is not absolute.");
		}
		if (!folder.exists()) {
			this.log.error("Visor " + folderName + folder.getAbsolutePath() + " does not exist.");
			return false;
		}
		if (!folder.isDirectory()) {
			this.log.error("Visor " + folderName + folder.getAbsolutePath() + " is not a folder.");
			return false;
		}
		return true;
	}


	/**
	 * @return the env
	 */
	public Map<String, String> getEnv() {
		if (this.env == null) {
			this.env = new HashMap<>();
		}
		return this.env;
	}


	/**
	 * @param env
	 *            the env to set
	 */
	public void setEnv(final Map<String, String> env) {
		this.env = env;
	}


	/**
	 * @return the datasetsFolder
	 */
	public File getDatasetsFolder() {
		return this.datasetsFolder;
	}


	/**
	 * @param datasetsFolder
	 *            the datasetsFolder to set
	 */
	public void setDatasetsFolder(final File datasetsFolder) {
		this.datasetsFolder = datasetsFolder;
	}


	/**
	 * @return the removeKeyframePrefix
	 */
	public boolean isRemoveKeyframePrefix() {
		return this.removeKeyframePrefix;
	}


	/**
	 * @param removeKeyframePrefix
	 *            the removeKeyframePrefix to set
	 */
	public void setRemoveKeyframePrefix(final boolean removeKeyframePrefix) {
		this.removeKeyframePrefix = removeKeyframePrefix;
	}


	/**
	 * @return the commandLine
	 */
	public String getCommandLine() {
		return this.commandLine;
	}


	/**
	 * @param commandLine
	 *            the commandLine to set
	 */
	public void setCommandLine(final String commandLine) {
		this.commandLine = commandLine;
	}


	/**
	 * @return the workingDirectory
	 */
	public File getWorkingDirectory() {
		return this.workingDirectory;
	}


	/**
	 * @param workingDirectory
	 *            the workingDirectory to set
	 */
	public void setWorkingDirectory(final File workingDirectory) {
		this.workingDirectory = workingDirectory;
	}


	/**
	 * @return the exitValue
	 */
	public int getExitValue() {
		return this.exitValue;
	}


	/**
	 * @param exitValue
	 *            the exitValue to set
	 */
	public void setExitValue(final int exitValue) {
		this.exitValue = exitValue;
	}


	/**
	 * @return the timeout
	 */
	public long getTimeout() {
		return this.timeout;
	}


	/**
	 * @param timeout
	 *            the timeout to set
	 */
	public void setTimeout(final long timeout) {
		this.timeout = timeout;
	}

}
