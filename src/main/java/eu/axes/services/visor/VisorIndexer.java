package eu.axes.services.visor;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;
import org.ow2.weblab.rdf.Value;

import eu.axes.services.visor.cmd.CmdLineExecutor;
import eu.axes.utils.AxesAnnotator;

/**
 * This class is in charge of copying frames images into a folder dedicated to the indexing by Visor and either call an incremental indexing script or doing nothing. In the latter case, the commit has
 * to be done either manually or through VisorCommiter.
 *
 * @author ymombrun
 * @date 2014-02-24
 */
public class VisorIndexer implements Indexer {


	private static final String LIST_IMAGES_FILE = "LIST_IMAGES_FILE";


	private final Log log;


	private final ContentManager contentManager;


	private final VisorConfig configuration;


	/**
	 * @param configuration
	 *            The visor configuration provided
	 */
	public VisorIndexer(final VisorConfig configuration) {
		this.log = LogFactory.getLog(this.getClass());
		this.contentManager = ContentManager.getInstance();
		if (!configuration.isValid()) {
			this.log.fatal("Visor configuration is not valid (or Visor is not installed).");
		}
		this.configuration = configuration;
		this.log.info("Service " + this.getClass().getSimpleName() + " succesfully initialised.");
	}



	@Override
	public IndexReturn index(final IndexArgs args) throws InvalidParameterException, UnexpectedException {

		this.log.trace("Early call to index method.");

		final String collectionId = this.checkArgs(args);

		this.log.trace("Index method called for " + args.getResource().getUri() + ".");
		final List<Image> images = ResourceUtil.getSelectedSubResources(args.getResource(), Image.class);
		final String uri = args.getResource().getUri();
		if (images.size() < 1) {
			this.log.info("Not a single image in document " + uri + ". Skipping it.");
			return new IndexReturn();
		}

		this.log.info("Start indexing " + images.size() + " images from Document " + uri + " into collection " + collectionId + ".");

		final File videoFolder = this.readVideoFolder(args.getResource(), collectionId);

		if (FileUtils.listFiles(videoFolder, new String[] { "jpg" }, true).size() >= images.size()) {
			this.log.info("It seems that more than " + images.size() + " jpg yet exist in the folder " + videoFolder.getAbsolutePath() + " dedicated to the document " + uri
					+ ". Nothing will be done.");
			return new IndexReturn();
		}

		final String normalisedParent = FilenameUtils.normalizeNoEndSeparator(this.configuration.getDatasetsFolder().getAbsolutePath());
		final List<String> added = new LinkedList<>();
		for (final Image image : images) {
			final File target = this.storeImage(image, videoFolder, collectionId);
			if (target != null) {
				added.add(FilenameUtils.normalizeNoEndSeparator(target.getAbsolutePath()).replace(normalisedParent + '/', ""));
			}
		}
		if (added.isEmpty()) {
			this.log.info("Not a single new image from " + uri + " has been copied to the storage folder. Might either be a reprocessed resource or an error.");
		} else if (added.size() < images.size()) {
			this.log.warn((added.size() - images.size()) + " images haven't been copied to the storage folder for " + uri + ".");
		} else {
			this.log.info("Images from " + uri + " have been successfully copied to the storage folder.");
		}


		this.commit(added, args.getResource().getUri());

		return new IndexReturn();
	}


	private synchronized void commit(final List<String> stored, final String resourceUri) throws UnexpectedException {
		final File appendFile;
		try {
			appendFile = File.createTempFile("visor-list", ".txt");
		} catch (final IOException ioe) {
			final String msg = "Unable to create temporary file that list the new image to be added to the index for " + resourceUri + ".";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}
		try {
			FileUtils.writeLines(appendFile, stored);
		} catch (final IOException e) {
			final String msg = "An error occured writing to the temporary file that list the new image to be added to the index for " + resourceUri + " (" + appendFile.getAbsolutePath() + ").";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}

		final Map<String, String> substitutionMap = new HashMap<>();
		substitutionMap.put(VisorIndexer.LIST_IMAGES_FILE, FilenameUtils.normalizeNoEndSeparator(appendFile.getAbsolutePath()));

		CmdLineExecutor.executeCommandLine(this.configuration.getCommandLine(), substitutionMap, this.configuration.getEnv(), this.configuration.getWorkingDirectory(),
				this.configuration.getExitValue(), this.configuration.getTimeout());
	}



	/**
	 * @param image
	 *            The image to be stored
	 * @param videoFolder
	 *            The folder of the videos
	 * @param collectionId
	 *            The collection id
	 * @return Where the image has been stored of null if an error occurred
	 */
	private File storeImage(final Image image, final File videoFolder, final String collectionId) {
		final AxesAnnotator aa = new AxesAnnotator(image);
		final Value<String> shotIdValue = aa.readShotId();
		if (!shotIdValue.hasValue()) {
			this.log.warn("No shot id annotated on Image " + image.getUri() + ".");
			return null;
		}
		final String shotId = shotIdValue.firstTypedValue();
		final Value<String> frameIdValue = aa.readFrameId();
		if (!frameIdValue.hasValue()) {
			this.log.warn("No frame id annotated on Image " + image.getUri() + ".");
			return null;
		}

		final File shotDir = new File(videoFolder, shotId);
		if (!shotDir.exists() && !shotDir.mkdirs()) {
			this.log.warn("Unable to create folder " + shotDir.getAbsolutePath() + " dedicated to image " + image.getUri() + ".");
			return null;
		}

		final String tempFrameId = frameIdValue.firstTypedValue();
		final String frameId;
		if (this.configuration.isRemoveKeyframePrefix()) {
			frameId = tempFrameId.replace("keyframe", "").replace("kf", "").replace("k", "");
		} else {
			frameId = tempFrameId;
		}

		final File frameFile = new File(shotDir, frameId + ".jpg");
		File contentFile;
		try {
			contentFile = this.contentManager.readNormalisedContent(image);
		} catch (final WebLabCheckedException wlce) {
			this.log.debug("No normalisedContent on image " + image.getUri() + ". Trying with native.");
			try {
				contentFile = this.contentManager.readNativeContent(image);
			} catch (final WebLabCheckedException e) {
				this.log.warn("Neither normalised nor native content on image " + image.getUri() + ".");
				return null;
			}
		}
		if (frameFile.exists() && (frameFile.length() == contentFile.length()) && (frameFile.lastModified() == contentFile.lastModified())) {
			this.log.debug("File " + frameFile.getAbsolutePath() + " was already copied. Nothing done here.");
			return null;
		}
		try {
			FileUtils.copyFile(contentFile, frameFile);
		} catch (final IOException ioe) {
			this.log.warn("An error occurs while copying image from " + contentFile.getAbsolutePath() + " to " + frameFile.getAbsolutePath() + ".", ioe);
			return null;
		}
		return frameFile;
	}


	/**
	 * @param collectionId
	 *            The id of the collection
	 * @param res
	 *            The resource on which open an AxesAnnotator
	 * @return The file were should be stored the video images
	 * @throws UnexpectedException
	 *             If a file exists where a folder should be found or if the folder cannot be created
	 * @throws InvalidParameterException
	 *             If the received resource has no video id
	 */
	private File readVideoFolder(final Resource res, final String collectionId) throws InvalidParameterException, UnexpectedException {

		final File collectionFolder = new File(this.configuration.getDatasetsFolder(), collectionId);
		if (!collectionFolder.exists() && !collectionFolder.mkdirs()) {
			final String msg = "Unable to create folder " + collectionFolder.getAbsolutePath() + " dedicated to the collection " + collectionId + ".";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}
		if (!collectionFolder.isDirectory()) {
			final String msg = "A file already exist were the folder " + collectionFolder.getAbsolutePath() + " dedicated to the collection " + collectionId + " should have been created.";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}

		final AxesAnnotator aa = new AxesAnnotator(res);

		final Value<String> videoIdValue = aa.readVideoId();
		final String videoId;
		if (!videoIdValue.hasValue()) {
			final String msg = "No video id annotated on Document " + res.getUri() + ".";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		if ((videoIdValue.size() > 1) && (new HashSet<>(videoIdValue.getValues()).size() > 1)) {
			this.log.warn("More than one videoId value. Using first one.");
		}
		videoId = videoIdValue.firstTypedValue();

		final File videoFolder = new File(collectionFolder, videoId);
		if (!videoFolder.exists() && !videoFolder.mkdirs()) {
			final String msg = "Unable to create folder " + videoFolder.getAbsolutePath() + " dedicated to the document " + res.getUri() + ".";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}
		if (!videoFolder.isDirectory()) {
			final String msg = "A file yet exist were the folder " + videoFolder.getAbsolutePath() + " dedicated to the document " + res.getUri() + " should have been created.";
			this.log.error(msg);
			throw ExceptionFactory.createUnexpectedException(msg);
		}

		return videoFolder;
	}



	/**
	 * Checks that index args is valid.
	 *
	 * @param args
	 *            The IndexArgs sent to the index method
	 * @return The collectiond Id
	 * @throws InvalidParameterException
	 *             If args is null, does not contain a Document or when the document's collection id is not handled by the configuration.
	 */
	private String checkArgs(final IndexArgs args) throws InvalidParameterException {
		if (args == null) {
			final String msg = "IndexArgs was null.";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String msg = "Resource in IndexArgs was null.";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		if (!(res instanceof Document)) {
			final String msg = "Resource in IndexArgs was not a Document but a " + res.getClass().getSimpleName() + ".";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		final Value<String> collectionIds = new AxesAnnotator(res).readCollectionId();
		if (!collectionIds.hasValue()) {
			final String msg = "Resource in IndexArgs has no collection id annotated.";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		if ((collectionIds.size() > 1) && (new HashSet<>(collectionIds.getValues()).size() > 1)) {
			this.log.warn("More than one collectionId value. Using first one.");
		}
		return collectionIds.firstTypedValue();
	}

}
