package eu.axes.services.visor.cmd;

import java.io.File;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.UnexpectedException;


/**
 * A simple class used to wrap command line call.
 *
 * @author ymombrun
 * @date 2014-02-24
 */
public class CmdLineExecutor {



	/**
	 * @param cmdStr
	 *            The command line to execute
	 * @param substitutionmap
	 *            The substitution map that will replace placeholders in <code>cmdStr</code> by the values of this map
	 * @param envMap
	 *            The environment variables to be set prior to trigger the command line
	 * @param workingDirectory
	 *            The directory where to execute the command line
	 * @param exitValue
	 *            The expected normal exit value
	 * @param timeout
	 *            The time to wait for normal completion or arbort
	 * @throws UnexpectedException
	 *             If the command line cannot be executed, if a timeout occurs or if it returns an non normal exit code.
	 */
	public static void executeCommandLine(final String cmdStr, final Map<String, ?> substitutionmap, final Map<String, String> envMap, final File workingDirectory, final int exitValue,
			final long timeout) throws UnexpectedException {
		final CommandLine cmdLine = CommandLine.parse(cmdStr, substitutionmap);
		final DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		final ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);

		final Executor executor = new DefaultExecutor();
		executor.setExitValue(exitValue);
		executor.setWatchdog(watchdog);
		executor.setWorkingDirectory(workingDirectory);

		try (final CommonsLoggingStream stdout = new CommonsLoggingStream(false); final CommonsLoggingStream stderr = new CommonsLoggingStream(true);) {
			executor.setStreamHandler(new PumpStreamHandler(stdout, stderr));

			final Log log = LogFactory.getLog(CmdLineExecutor.class);
			log.info("Calling command line " + cmdLine.toString());


			try {
				executor.execute(cmdLine, envMap, resultHandler);
			} catch (final Exception e) {
				final String message = "An error occurred executing the command line " + cmdLine;
				log.error(message, e);
				throw ExceptionFactory.createUnexpectedException(message, e);
			}

			try {
				resultHandler.waitFor();
			} catch (final Exception e) {
				final String message = "An error occurred executing the command line " + cmdLine;
				log.error(message, e);
				throw ExceptionFactory.createUnexpectedException(message, e);
			}

			log.info("Command line " + cmdLine.toString() + " just finished and returned " + resultHandler.getExitValue() + ".");

			if (resultHandler.getExitValue() != exitValue) {
				final String message = "Exit value for " + cmdLine.toString() + " is " + resultHandler.getExitValue() + ". " + exitValue + " was expected for normal execution.";
				log.error(message);
				throw ExceptionFactory.createUnexpectedException(message);
			}
		}

	}


}
