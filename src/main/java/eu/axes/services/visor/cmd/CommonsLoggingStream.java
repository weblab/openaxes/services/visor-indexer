package eu.axes.services.visor.cmd;

import java.io.IOException;

import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This class is an Output Stream handler in charge of writing lines to the commons-logging logger inside.
 *
 * @author ymombrun
 * @date 2014-02-24
 */
public class CommonsLoggingStream extends LogOutputStream {


	/**
	 * The logger used inside
	 */
	private final Log log;


	/**
	 * Whether or not to log as error
	 */
	private final boolean error;


	/**
	 * @param error
	 *            Whether or not to log as error
	 */
	public CommonsLoggingStream(final boolean error) {
		this.error = error;
		this.log = LogFactory.getLog(this.getClass());
	}


	/**
	 * @param line
	 *            The line to log
	 * @param level
	 *            Not used
	 */
	@Override
	protected void processLine(final String line, final int level) {
		if (this.error) {
			this.log.error(line);
		} else {
			this.log.debug(line);
		}
	}


	@Override
	public void close() {
		try {
			super.close();
		} catch (final IOException ioe) {
			// Ignore closing error safely.
		}
	}

}
